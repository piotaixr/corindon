# frozen_string_literal: true

require('test_helper')

CustomError = Class.new(StandardError)

describe(Corindon::Result::Failure) do
  before do
    @error = StandardError.new('Error')
    @result = Corindon::Result::Failure.new(@error)
  end

  it('return false for success?') do
    refute(@result.success?)
  end

  it('returns true for failure?') do
    assert(@result.failure?)
  end

  it('contains the error provided to constructor') do
    assert_equal(@error, @result.error)
  end

  describe(:and_then) do
    it('returns self for Failures') do
      called = false

      r = @result.and_then do |_value|
        called = true
      end

      refute(called)
      assert_equal(@result, r)
    end
  end

  describe(:unwrap!) do
    it('raises the error contained in a Failure') do
      error = assert_raises(Corindon::Result::Errors::UnwrapError) do
        Corindon::Result::Failure.new(CustomError.new).unwrap!
      end

      assert(error.value.is_a?(CustomError))
    end
  end
end
