# frozen_string_literal: true

require('test_helper')

describe(Corindon::Result::Success) do
  before do
    @result = Corindon::Result::Success.new(42)
  end

  it('return true for success?') do
    assert(@result.success?)
  end

  it('returns false for failure?') do
    refute(@result.failure?)
  end

  it('contains the value provided to constructor') do
    assert_equal(42, @result.value)
  end

  describe(:and_then) do
    it('returns the value returned by the block for Success') do
      called = false

      r = @result.and_then do |value|
        called = true
        assert_equal(42, value)

        Corindon::Result::Success.new(12)
      end

      assert(called)
      assert_instance_of(Corindon::Result::Success, r)
      assert_equal(12, r.value)
    end

    it('catches StandardErrors when raised in the given block') do
      assert_instance_of(Corindon::Result::Failure, @result.and_then { raise(StandardError.new) })
    end

    it('returns a Failure if the value returned by the block is not a Result') do
      assert_instance_of(Corindon::Result::Failure, @result.and_then { 10 })
    end
  end

  describe(:unwrap!) do
    it('should return the wrapped value') do
      assert_equal(42, Corindon::Result::Success.new(42).unwrap!)
    end
  end
end
