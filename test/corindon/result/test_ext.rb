# frozen_string_literal: true

require('test_helper')

describe(Corindon::Result::Ext) do
  using(Corindon::Result::Ext)

  describe(:rescue_failure) do
    it('returns the value returned by the passed block') do
      assert_equal(42, rescue_failure { 42 })
    end

    it('wraps StandardErrors in a Failure object') do
      result = rescue_failure { raise(StandardError.new) }

      assert_instance_of(Corindon::Result::Failure, result)
    end
  end

  describe(:Failure) do
    it('is a shortcut to build Failure objects') do
      @result = Failure(StandardError.new)

      assert_instance_of(Corindon::Result::Failure, @result)
    end
  end

  describe(:Success) do
    it('is a shortcut to build Success objects') do
      @result = Success(42)

      assert_instance_of(Corindon::Result::Success, @result)
    end
  end
end
