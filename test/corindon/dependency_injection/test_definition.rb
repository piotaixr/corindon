# frozen_string_literal: true

require('test_helper')

class NoOpInjector
  def resolve(args)
    args
  end
end

describe(Corindon::DependencyInjection::Definition) do
  describe('build') do
    it('should call new if built only with a class') do
      assert_instance_of(Object, Corindon::DependencyInjection::Definition.new(Object).build(NoOpInjector.new))
    end

    it('should pass args and kwargs to the inspector to be resolved') do
      klass = Minitest::Mock.new
      klass.expect(:is_a?, true, [Class])
      klass.expect(:new, Object.new, [:resolved, { key: :value }])

      injector = Minitest::Mock.new
      injector.expect(:resolve, [:resolved], [[42]])
      injector.expect(:resolve, { key: :value }, [{ c: 27 }])

      Corindon::DependencyInjection::Definition.new(klass, args: [42], kwargs: { c: 27 }).build(injector)

      injector.verify
      klass.verify
    end

    it('should call the methods listed in calls and resolve their arguments beforehand') do
      object = Minitest::Mock.new
      object.expect(:my_method, nil, [:resolved, { key: :value }])
      klass = Minitest::Mock.new
      klass.expect(:is_a?, true, [Class])
      klass.expect(:new, object, [])

      injector = Minitest::Mock.new
      injector.expect(:resolve, [], [[]])
      injector.expect(:resolve, {}, [{}])
      injector.expect(:resolve, [:resolved], [[42]])
      injector.expect(:resolve, { key: :value }, [{ c: 27 }])

      Corindon::DependencyInjection::Definition.new(klass, calls: [[:my_method, [42], { c: 27 }]]).build(injector)

      injector.verify
      klass.verify
      object.verify
    end
  end
end
