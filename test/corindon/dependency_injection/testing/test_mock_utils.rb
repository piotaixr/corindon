# frozen_string_literal: true

describe(Corindon::DependencyInjection::Testing::MockUtils) do
  using(Corindon::DependencyInjection::Testing::MockUtils)

  before do
    @container = Corindon::DependencyInjection::Container.new
  end

  it('should add the definition to the container if it does not exist') do
    @container.mock_definition(String, 42)

    assert(@container.has?(String))
  end

  it('should return the provided value instead of building the service') do
    @container.mock_definition(String, 42)

    assert_equal(42, @container.get(String))
  end

  it('keeps the mocks when dup-ing the container') do
    @container.mock_definition(String, 42)

    assert_equal(42, @container.dup.get(String))
  end
end
