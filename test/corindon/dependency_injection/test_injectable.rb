# frozen_string_literal: true

require('test_helper')

class DummyClass
end

describe(Corindon::DependencyInjection::Injectable) do
  describe(:injectable) do
    using(Corindon::DependencyInjection::Injectable)

    before do
      @klass = DummyClass
    end

    it('defines the :definition method and includes Injectable') do
      @klass.injectable

      assert(@klass.respond_to?(:definition))
      assert(@klass.is_a?(Corindon::DependencyInjection::Injectable))
    end

    it('allows to specify a service and a method as factory for a service') do
      token = @klass.factory(String, :dup)

      assert(token.is_a?(Corindon::DependencyInjection::Token::ServiceFactoryToken))
      assert_equal(String, token.service)
      assert_equal(:dup, token.method)
    end

    it('allows to define named definitions') do
      service = @klass.make_definition('custom_name', String)

      assert(service.is_a?(Corindon::DependencyInjection::Definition))
      assert_equal('dummyclass.custom_name', service.id)
      assert_equal(String, service.object_source)
    end

    it('allow to create a parameter token') do
      param = @klass.make_parameter('name')

      assert(param.is_a?(Corindon::DependencyInjection::Token::ParameterToken))
      assert_equal('dummyclass.name', param.key)
    end
  end
end
