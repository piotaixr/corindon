# frozen_string_literal: true

require('test_helper')

class DummyService
end

class Access
  attr_accessor(:value)
end

describe(Corindon::DependencyInjection::ParameterBag) do
  before do
    @bag = Corindon::DependencyInjection::ParameterBag.new
  end

  describe(:has?) do
    it('has_parameters return false if no parameter') do
      refute(@bag.has?(String))
    end

    it('returns true if the parameter exists') do
      @bag.set('key', 'value')

      assert(@bag.has?('key'))
    end
  end

  describe(%i[set get]) do
    it('remembers the value of a parameter') do
      @bag.set(String, 'a string')

      assert_equal('a string', @bag.get(String))
    end

    it('considers Class<String> and "String" to be the same') do
      @bag.set(String, 'value')

      assert(@bag.has?('String'))
    end

    it('allow to define parameters with a ParameterToken') do
      token = Corindon::DependencyInjection::Token::ParameterToken.new(key: 'String')
      @bag.set(token, 'value')

      assert(@bag.has?('String'))
    end
  end
end
