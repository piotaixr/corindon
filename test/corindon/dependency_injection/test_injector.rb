# frozen_string_literal: true

require('test_helper')

describe(Corindon::DependencyInjection::Injector) do
  describe(:resolve) do
    before do
      @container = Minitest::Mock.new
      @injector = Corindon::DependencyInjection::Injector.new(container: @container)
    end

    after do
      @container.verify
    end

    it('resolves simple key') do
      @container.expect(:get, :value, [:key])

      assert_equal(:value, @injector.resolve(:key))
    end

    it('resolves arrays') do
      @container.expect(:get, :value, [:key])
      @container.expect(:get, :value2, [:key2])

      assert_equal(%i[value value2], @injector.resolve(%i[key key2]))
    end

    it('resolves hash values') do
      @container.expect(:get, :value, [:key])

      assert_equal({ k: :value }, @injector.resolve({ k: :key }))
    end

    it('resolves injection tokens') do
      token = Minitest::Mock.new(Corindon::DependencyInjection::Token::InjectionToken.new)
      token.expect(:resolve, 42, [{ injector: @injector }])

      assert_equal(42, @injector.resolve(token))

      token.verify
    end
  end
end
