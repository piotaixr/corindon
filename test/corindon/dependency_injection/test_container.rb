# frozen_string_literal: true

require('test_helper')

module Corindon
  module DependencyInjection
    class DummyService
      using(Injectable)

      CUSTOM_DUMMY = make_definition('dummy', DummyService)
    end

    class RaisesOnInitialize
      using(Injectable)
      injectable

      def initialize
        raise(StandardError.new('I cannot be built'))
      end
    end

    class Access
      attr_accessor(:value)
    end

    describe(Container) do
      before do
        @container = Container.new
      end

      describe(:has?) do
        it('should return false if the definition is not defined') do
          refute(@container.has?(String))
        end

        it('should return true if definition is registred') do
          @container.add_definition(String)

          assert(@container.has?(String))
        end

        it('allows to change the service id when registering the service') do
          @container.add_definition(String) { id('new_id') }

          refute(@container.has?(String))
          assert(@container.has?('new_id'))
        end
      end

      describe(:dup) do
        it('creates a duplicate of the container that contains the same definitions') do
          @container.add_definition(String)

          dup = @container.dup
          assert(dup.has?(String))
        end

        it('does not duplicate services') do
          @container.add_definition(DummyService)
          dummy1 = @container.get(DummyService)

          dup = @container.dup
          dummy2 = dup.get(DummyService)
          refute_equal(dummy1, dummy2)
        end

        it('duplicates parameters') do
          @container.set_parameter('key', 42)
          duped = @container.dup

          assert_equal(42, duped.parameter('key'))
        end
      end

      describe(:clear) do
        it('clears only created services') do
          @container.add_definition(DummyService)
          svc = @container.get(DummyService)
          @container.clear

          assert(@container.has?(DummyService))
          refute_equal(svc, @container.get(DummyService))
        end
      end

      describe(:add_definition) do
        it('passes along the context into the DSL block') do
          @container.add_definition(String, context: 'a value') do |val|
            args(value(val))
          end

          assert_equal('a value', @container.get(String))
        end

        it('transforms a hash into a Struct for ease of use') do
          @container.add_definition(Access, context: { a: :value }) do |ctx|
            call(:value=, value(ctx))
          end

          access = @container.get(Access)

          assert_instance_of(OpenStruct, access.value)
          assert(access.value.respond_to?(:a))
          assert_equal(:value, access.value.a)
        end

        it('allows directly providing a definition') do
          definition = Definition.new(String, id: 'specific_id')

          @container.add_definition(definition)

          assert(@container.has?('specific_id'))
        end
      end

      describe(:parameters) do
        it('has_parameters return false if no parameter') do
          refute(@container.parameter?(String))
        end

        it('keep separate definitions and parameters') do
          @container.add_definition(Hash)
          @container.set_parameter(String, '')

          refute(@container.has?(String))
          refute(@container.parameter?(Hash))
        end

        it('remembers the value of a parameter') do
          @container.set_parameter(String, 'a string')

          assert_equal('a string', @container.parameter(String))
        end
      end

      describe(:on_service_built) do
        it('allows to register a callback run when a service is built') do
          called = false
          service_given = nil
          @container.on_service_built(proc do |service, container|
            called = true
            service_given = service
            assert_equal(@container, container)
          end)

          @container.add_definition(DummyService)

          svc = @container.get(DummyService)

          assert(called)
          assert_equal(svc, service_given)
        end
      end

      describe(:tagged) do
        it('should register definition by the tags they provide') do
          assert_empty(@container.tagged('string'))

          @container.add_definition(String) do
            tag('string')
          end

          assert_equal(1, @container.tagged('string').size)
        end
      end

      describe(:get) do
        using(Injectable)

        before do
          @container.add_definition(String)
        end

        it('should build a service that was previously registered') do
          assert_equal('', @container.get(String))
        end

        it('should successfully build an injectable class') do
          klass = Class.new do
            injectable(String)

            attr_reader(:param)

            def initialize(param)
              @param = param
            end
          end

          object = @container.get(klass)

          assert_equal('', object.param)
        end
      end

      describe(:nested_definitions) do
        it('reuses services when definitions depends on other definitions') do
          container = Container.new
          container.add_definition(DummyService::CUSTOM_DUMMY)
          container.add_definition(Access) do
            call(:value=, DummyService::CUSTOM_DUMMY)
          end

          dummy = container.get(DummyService::CUSTOM_DUMMY)

          assert_same(dummy, container.get(Access).value)
        end
      end

      describe(:exceptions) do
        before do
          @container = Container.new
        end

        it('raises a UnknownServiceError if it does not have a definition') do
          exception = assert_raises(Errors::UnknownServiceError) { @container.get(String) }
          assert_equal(String, exception.definition)
        end

        it('raises a ServiceResolutionError when a services raise an exception on initialization') do
          @container.add_definition(RaisesOnInitialize)

          exception = assert_raises(Errors::ServiceResolutionError) { @container.get(RaisesOnInitialize) }
          assert_equal(RaisesOnInitialize.definition.id, exception.definition.id)
          assert_instance_of(Errors::ObjectInitializationError, exception.parent)
          assert_equal('I cannot be built', exception.parent.parent.message)
        end

        it('contains the chain of values being resolved') do
          @container.add_definition(RaisesOnInitialize)
          @container.add_definition(Access) do
            call(:value=, [RaisesOnInitialize, DummyService, DummyService::CUSTOM_DUMMY])
          end

          exception = assert_raises(Errors::ServiceResolutionError) { @container.get(Access) }
          assert_equal(Access, exception.definition.object_source)

          nested = exception.parent
          assert_instance_of(Errors::CallInjectionError, nested)
        end
      end
    end
  end
end
