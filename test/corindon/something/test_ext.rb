# frozen_string_literal: true

require('test_helper')

describe(Corindon::Something::Ext) do
  describe('When the refinment is used') do
    using(Corindon::Something::Ext)

    it('should patch the environment') do
      refute(nil.sth?)
      assert(0.sth?)
      assert(''.sth?)
      assert([].sth?)
      assert({}.sth?)
      assert(Set.new.sth?)
    end
  end
end
