# frozen_string_literal: true

require('bundler/setup')
Bundler.require(:default, :development)

require('minitest/autorun')
require('minitest/reporters')

require('corindon')

class SpecReporter < Minitest::Reporters::SpecReporter
  def record_print_status(test)
    test_name = test.name.gsub(/^test_: /, 'test:')
    print_colored_status(test)
    print(pad_test(test_name))
    print(format(' (%.2fs)', test.time)) unless test.time.nil?
    puts
  end

  private

    def pad_test(str)
      if ENV.key?('CI')
        super
      else
        pad(format("%-#{[TEST_SIZE, IO.console.winsize[1] - 15].max}s", str), TEST_PADDING)
      end
    end
end

Minitest::Reporters.use!(SpecReporter.new)
