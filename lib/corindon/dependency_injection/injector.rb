# frozen_string_literal: true

module Corindon
  module DependencyInjection
    class Injector
      # @return [Container]
      attr_reader(:container)

      # @param [Container] container
      def initialize(container:)
        @container = container
      end

      # @param [Object] value
      def resolve(value)
        if value.is_a?(Array)
          wrap_exception(value) { value.map(&method(:resolve)) }
        elsif value.is_a?(Hash)
          wrap_exception(value) { value.transform_values(&method(:resolve)) }
        elsif value.is_a?(Token::InjectionToken)
          wrap_exception(value) { value.resolve(injector: self) }
        else
          container.get(value)
        end
      end

      private

        def wrap_exception(value, &block)
          block.call
        rescue StandardError => e
          raise(Errors::ServiceResolutionError.new(value, e))
        end
    end
  end
end
