# frozen_string_literal: true

module Corindon
  module DependencyInjection
    module Errors
      class ObjectInitializationError < ErrorBase
        include(ErrorFormatterMixin)

        attr_reader(:source)
        attr_reader(:parent)

        def initialize(source, parent = nil)
          super()

          @source = source
          @parent = parent
        end

        def message
          <<~TEXT
            Error while initializing service: call of #{source[1]} on #{source[0]} raised:#{' '}
            #{indent(parent_message(parent))}
          TEXT
        end
      end
    end
  end
end
