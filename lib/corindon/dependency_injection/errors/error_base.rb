# frozen_string_literal: true

module Corindon
  module DependencyInjection
    module Errors
      class ErrorBase < StandardError
        def initialize
          super('')
        end
      end
    end
  end
end
