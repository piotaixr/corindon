# frozen_string_literal: true

module Corindon
  module DependencyInjection
    module Errors
      module ErrorFormatterMixin
        private

          def indent(str)
            str.gsub(/^/, '  ')
          end

          def parent_message(exception)
            if exception.nil?
              'Unknown error'
            else
              exception.message
            end
          end

          def id_for(definition_like)
            if definition_like.is_a?(Injectable)
              id_for(definition_like.definition)
            elsif definition_like.is_a?(Definition)
              definition_like.id
            elsif definition_like.is_a?(Array)
              "[#{definition_like.map(&method(:id_for)).join(', ')}]"
            else
              definition_like.class.name
            end
          end
      end
    end
  end
end
