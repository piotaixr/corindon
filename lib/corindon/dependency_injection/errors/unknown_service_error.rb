# frozen_string_literal: true

module Corindon
  module DependencyInjection
    module Errors
      class UnknownServiceError < ErrorBase
        attr_reader(:definition)

        def initialize(definition)
          super()

          @definition = definition
        end

        def message
          "The service #{definition} is unknown"
        end
      end
    end
  end
end
