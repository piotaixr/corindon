# frozen_string_literal: true

module Corindon
  module DependencyInjection
    module Errors
      class CallInjectionError < ErrorBase
        include(ErrorFormatterMixin)

        attr_reader(:definition)
        attr_reader(:parent)
        attr_reader(:method)

        def initialize(definition, method, parent = nil)
          super()

          @definition = definition
          @parent = parent
          @method = method
        end

        def message
          <<~TEXT
            Call to #{method} raised an exception:
            #{indent(parent_message(parent))}
          TEXT
        end
      end
    end
  end
end
