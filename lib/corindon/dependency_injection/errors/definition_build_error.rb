# frozen_string_literal: true

module Corindon
  module DependencyInjection
    module Errors
      class DefinitionBuildError < ErrorBase
        attr_reader(:definition)

        def initialize(definition)
          super()

          @definition = definition
        end
      end
    end
  end
end
