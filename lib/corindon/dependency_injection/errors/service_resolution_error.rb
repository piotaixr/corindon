# frozen_string_literal: true

module Corindon
  module DependencyInjection
    module Errors
      class ServiceResolutionError < DefinitionBuildError
        include(ErrorFormatterMixin)

        attr_reader(:parent)

        def initialize(definition, parent = nil)
          super(definition)

          @parent = parent
        end

        def message
          <<~TEXT
            Can't build #{id_for(definition)}:
            #{indent(parent_message(parent))}
          TEXT
        end
      end
    end
  end
end
