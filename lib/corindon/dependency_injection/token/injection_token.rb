# frozen_string_literal: true

module Corindon
  module DependencyInjection
    module Token
      class InjectionToken
        # @param [Injector] injector
        def resolve(injector:) end
      end
    end
  end
end
