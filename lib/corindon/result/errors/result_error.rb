# frozen_string_literal: true

module Corindon
  module Result
    module Errors
      class ResultError < StandardError
      end
    end
  end
end
