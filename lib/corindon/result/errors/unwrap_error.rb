# frozen_string_literal: true

module Corindon
  module Result
    module Errors
      class UnwrapError < ResultError
        # @return [Object]
        attr_reader(:value)

        # @param [Object] value
        def initialize(value)
          super("Unwrap called on a Failure containing: #{value}")

          @value = value
        end
      end
    end
  end
end
