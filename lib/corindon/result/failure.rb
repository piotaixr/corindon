# frozen_string_literal: true

module Corindon
  module Result
    class Failure < Result
      # @return [Object]
      attr_reader(:error)

      # @param [Object] error
      def initialize(error)
        super()

        @error = error
      end

      # @raise [Exception]
      def unwrap!
        raise(Errors::UnwrapError.new(error))
      end

      # @return [Boolean]
      def failure?
        true
      end
    end
  end
end
