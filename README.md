# Corindon

Corindon is a collection of tools for Ruby

## Components

### DependencyInjection

This component implements a dependency injection container.

#### TODO

- Make the container behave correctly in Rails whith reloading in development mode (and with zeitwerk reloading too!)
    - Ability to clear the services in the container
    - Have definition behave nicely with class reloading (constantize?)
- Extensions allowing to define a group of sevices
- Container scope allowing to define additional services that are only valid in this scope and removed when exiting it.
- more...

#### Register a service

Services can be registered in multiple ways:

1. A Class having a constructor with no parameters

``` ruby
container = Corindon::DependencyInjection::Container.new

container.register(String)
```

2. A Class with an injection configuration

Configuration for the injector can be done using the `Corindon::DependencyInjection::Injectable` refinment:
``` ruby
using Corindon::DependencyInjection::Injectable

class Service
    injectable String, a_kwarg: String

    def initialize(str, a_kwarg:)
        # This will be called with ["", a_kwarg: ""] according to the injection configuration
    end
end

container = Corindon::DependencyInjection::Container.new
container.add_dependency(Service)
```

3. Provide injection configuration when registering the service

``` ruby
class Service
    def initialize(str, a_kwarg:)
        # This will be called with ["", a_kwarg: ""] according to the injection configuration
    end

    def a_method(arg)
        puts "a_method called with #{arg}"
    end
end


container = Corindon::DependencyInjection::Container.new
container.register(Service) do
    args String, a_kwarg: String
end
```

#### Instantiate a service

Services are lazily created when first requested and the same instance is returned between two calls for the same service.

For this, use the `get` method:
``` ruby
container = Corindon::DependencyInjection::Container.new
# Register some services here...

instance = container.get(String) 
```

The `get` method can also instanciate any Class, provided it is `Injectable` without being registered in the container.
Note that, in this case, an new instance is returned each time.

#### Advanced injectable configuration

Available DSL methods:
- `def args(*args, **kwargs)`: Used to list the values that will be injected in the constructor
- `def call(method, *args, *kwargs)`: Used to add a method call that will be executed immediately after object initialization.
- `def tag(tag)`: Used to add a tag to the current service definition.
- `def id(id)`: Used to set the id for the service when added to a container instead of the String representation of th service class.
- `def anonymous!`: Used to mark this service as 'anonymous' allowing it to be registered multiple times with the same class as constructor. It is mainly to be used with tagged services. 
- `def tagged(tag)`: This returns an injection token that will be resolved by a collection of the services registered with the given tag.


The `injectable` refined method also has a 'long' version that allow to register calls on the instance after service initialization.
This feature is also available when providing injection configuration when registering the service.

``` ruby
using Corindon::DependencyInjection::Injectable

class Service
    injectable do
        args String, loaders: tagged('loader')
        
        # Resolves the Logger service and sets it on the Servoce instance on initialization
        call :logger=, Logger 
    end

    attr_accessor :logger

    def initialize(str, a_kwarg:)
        # This will be called with ["", a_kwarg: ""] according to the injection configuration
    end
end
```